module Main where

import Euterpea
import Scales
import Counterpoint
import Compose

getNotes :: [Int] -> [Music Pitch] -> [Music Pitch]
getNotes sg scale = map (\x -> scale !! x) sg

mkTriad :: Int -> [Int]
mkTriad n = [n, n+2, n+4]

mkSeventh :: Int -> [Int]
mkSeventh n = [n, n+2, n+4, n+6]

getChords :: Int -> [Music Pitch] -> Music Pitch
getChords degree scale = let dgs = mkSeventh degree
                             sev = getNotes dgs scale
                          in chord sev

makeChordProgression :: [Int] -> [Music Pitch] -> [Music Pitch]
makeChordProgression prog scale = let octs = sevenOctaves scale
                                      gc o = getChords (7 + (o-1)) octs
                                   in map gc prog


playMelodyWithChords :: (Dur -> [Music Pitch]) -> [Int] -> [Int] -> Music Pitch
playMelodyWithChords scale melody chords = let mel = getNotes melody $ scale qn
                                               chrds = makeChordProgression chords (scale wn)
                                            in line mel :=: line chrds

--how to compose 1:1 counterpoint
-- https://www.youtube.com/watch?v=b5PoTBOj7Xc

counterpoint :: [Music Pitch]
counterpoint = [
                    g 4 hn,
                    f 4 hn,
                    g 4 hn,
                    a 4 hn,
                    b 4 hn,
                    d 5 hn,
                    c 5 hn,
                    c 5 hn,
                    d 5 hn,
                    e 5 hn,
                    d 5 hn,
                    a 4 hn,
                    c 5 hn,
                    b 4 hn,
                    c 5 hn
               ]


-- by Fancois-Josepf Fetis (1784-1871)
cantusFirmus :: [Music Pitch]
cantusFirmus = [
                    c 4 hn,
                    d 4 hn,
                    e 4 hn,
                    c 4 hn,
                    g 4 hn,
                    f 4 hn,
                    e 4 hn,
                    a 4 hn,
                    g 4 hn,
                    c 4 hn,
                    d 4 hn,
                    f 4 hn,
                    e 4 hn,
                    d 4 hn,
                    c 4 hn
               ]

cantusf = toPitches cantusFirmus

main :: IO ()
main = do ctpComposed <- composeCounterpoint cantusFirmus
          print "cantus firmus"
          print "composed counterpoint"
          play $ line cantusFirmus
          play $ line ctpComposed
          play $ line cantusFirmus :=: line ctpComposed
