module Scales where

{-# LANGUAGE GeneralizedNewtypDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Euterpea
import qualified Data.Set as Set

data ScaleDegree = Tonic | Supertonic | Mediant | Subdominant | Dominant | Submediant | Leading
 deriving (Eq, Ord, Enum, Show)

whole :: Int
whole = 2
half :: Int
half = 1

major= [whole, whole, half, whole, whole, whole, half]
minor= [whole, half, whole, whole, half, whole, whole]

ionian =     [whole, whole, half, whole, whole, whole, half]
dorian =     [whole, half, whole, whole, whole, half, whole]
phrygian =   [half, whole, whole, whole, half, whole,whole]
lydian =     [whole, whole, whole, half, whole, whole, half]
mixolydian = [whole, whole, half, whole, whole, half, whole]
aeolian =    [whole, half, whole, whole, half, whole, whole]
locrian =    [half, whole, whole, half, whole, whole, whole]

majorPentatonic= [whole, whole, minorThird, whole, majorThird]
minorPentatonic= [minorThird, whole, whole, minorThird, whole]
mixolydianPentatonic= [whole, whole, minorThird, minorThird, whole]
phrygianPentatonic= [half, whole, minorThird, half, minorThird]
diminishedPentatonic= [whole, half, minorThird, half, minorThird]

unison :: Int
unison = 0

minorSecond :: Int
minorSecond = 1

majorSecond :: Int
majorSecond = 2

minorThird :: Int
minorThird = 3

majorThird :: Int
majorThird = 4

perfectFourth :: Int
perfectFourth = 5

tritone :: Int
tritone = 6

perfectFifth :: Int
perfectFifth = 7

minorSixth :: Int
minorSixth = 8

majorSixth :: Int
majorSixth = 9

minorSeventh :: Int
minorSeventh = 10

majorSeventh :: Int
majorSeventh = 11

octave :: Int
octave  = 12

--leaving out the perfectFourth from the list of perfect and consonant Intervals
perfection = Set.fromList [unison, octave, perfectFifth]
dissonantIntervals = Set.fromList [minorSecond, majorSecond, tritone, minorSeventh, majorSeventh]
consonantIntervals = Set.fromList [ unison, minorThird, majorThird, perfectFourth, perfectFifth, minorSixth, majorSixth, octave]


patternToSemitones :: [Int] -> [Int]
patternToSemitones pat = init $ scanl1 (+) (0:pat)


-- plays a scale with quarter notes
makeScale :: [Int] -> Pitch -> Rational -> [Music Pitch]
makeScale pattern p d = let f ap = note d (pitch (absPitch p + ap))
                            semis = patternToSemitones pattern
                         in map f semis

makeMajorScale :: Pitch -> Rational -> [Music Pitch]
makeMajorScale p d = makeScale major p d

makeMinorScale :: Pitch -> Rational -> [Music Pitch]
makeMinorScale p d = makeScale minor p d

cmajor = makeMajorScale (C,4)
dmajor = makeMajorScale (D,4)
emajor = makeMajorScale (E,4)
fmajor = makeMajorScale (F,4)
gmajor = makeMajorScale (G,4)
amajor = makeMajorScale (A,4)
bmajor = makeMajorScale (B,4)

cminor = makeMinorScale (C,4)
dminor = makeMinorScale (D,4)
eminor = makeMinorScale (E,4)
fminor = makeMinorScale (F,4)
gminor = makeMinorScale (G,4)
aminor = makeMinorScale (A,4)
bminor = makeMinorScale (B,4)

cminorPent = makeScale minorPentatonic (C,4)
cmajorPent = makeScale majorPentatonic (C,4)
cSmajorPent = makeScale majorPentatonic (Cs,4)

-- repeats a sequence of notes 8 times, raising an octave with each repeat
repeatOnOctave :: [Music Pitch] -> [Music Pitch]
repeatOnOctave music = let noteCount = length music
                           octs = map (* 12) [0..7]
                           shifts = map shiftPitches octs
                           scales = take 8 $ repeat music
                           shifted = zipWith (<$>) shifts scales
                        in concat shifted

sevenOctaves :: [Music Pitch] -> [Music Pitch]
sevenOctaves scl = let d1 = map (shiftPitches (-12)) scl
                       d2 = map (shiftPitches (-24)) scl
                       d3 = map (shiftPitches (-36)) scl
                       u1 = map (shiftPitches 12) scl
                       u2 = map (shiftPitches 24) scl
                       u3 = map (shiftPitches 36) scl
                    in d3 ++ d2 ++ d1 ++ scl ++ u1 ++ u2 ++ u3

getPitch :: Music Pitch -> (PitchClass,Int)
getPitch (Prim (Note d p)) = p

toPitches :: [Music Pitch] -> [Int]
toPitches  = map (absPitch . getPitch)


