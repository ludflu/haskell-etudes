module Counterpoint where

import           Data.List as DL
import qualified Data.Set as Set
import Scales

data MusicMotion = Contrary | Parallel | Similar | Oblique
    deriving (Show, Eq)

type Note = Int

isConsonantInterval :: Int -> Bool
isConsonantInterval i = Set.member i consonantIntervals

--The counterpoint must begin and end on a perfect consonance.
beginAndEndPerfectly :: [Int] -> [Int] -> Bool
beginAndEndPerfectly a b = let intervals = zipWith (-) a b
                               absIs = map abs intervals
                               begin = head absIs
                               end = last absIs
                               bperfect = Set.member begin perfection
                               eperfect = Set.member end perfection
                            in bperfect && eperfect

windows :: Int -> [a] -> [[a]]
windows n xs = map (take n) (DL.tails xs)

tuple2 :: [a] -> (a,a)
tuple2 ns = (head ns, (head . tail) ns)

diff :: (Int,Int) -> Int
diff (a,b) = b-a

--takes a series of notes, specificed in number of half steps (semitones)
-- returns a series of intervals, also in semitones. negative if pitch drops, positive if it rises
direction :: [Int] -> [Int]
direction ns = let prs = windows 2 ns
                   noEmptyPrs = filter (\x -> length x == 2) prs
                   tps = map (diff . tuple2) noEmptyPrs
                in tps

mostlySteps :: [Int] -> Bool
mostlySteps ns = let is = direction ns
                     absInts = map abs is
                     steps = filter (<=2) absInts
                     leaps = filter (>2) absInts
                  in length steps > length leaps

motionType :: Int -> Int -> MusicMotion
motionType a b
  | a == b       = Parallel   --same direction and interval
  | a>0 && b<0   = Contrary   --different direction
  | a<0 && b>0   = Contrary   --different direction
  | a==0 || b==0 = Oblique    --one note repeats
  | otherwise    = Similar    --same direction, different interval

motions :: [Int] -> [Int] -> [(Int,Int)]
motions a b = let adir = direction a
                  bdir = direction b
               in zip adir bdir
 
--takes two series of notes, specificed in semitones
--returns the motion type of each note movement with respect to each other
motionTypes :: [Int] -> [Int] -> [MusicMotion]
motionTypes a b = let pairs = motions a b
                   in map (uncurry motionType) pairs

--Contrary motion should dominate.
mostlyContrary :: [Int] -> [Int] -> Bool
mostlyContrary a b = let ms = motionTypes a b
                         contrary = filter (== Contrary) ms
                         notContrary = filter (/= Contrary) ms
                      in length contrary > length notContrary


relativeIntervals :: [Int] -> [Int] -> [Int]
relativeIntervals = zipWith (-)

absIntervals :: [Int] -> [Int] -> [Int]
absIntervals a b = let interval = relativeIntervals a b
                       absInt = map abs interval
                    in absInt


--Perfect consonances must be approached by oblique or contrary motion.
approachPerfection :: [Int] -> [Int] -> Bool
approachPerfection a b = let intervals = absIntervals a b
                             ms = motionTypes a b
                             ia = zip ms (tail intervals)
                             onlyPerfect = filter (\(m,i) -> Set.member i perfection) ia
                             onlyPerfectMotions = map fst onlyPerfect
                             contraryOrOblique m = (m == Contrary) || (m == Oblique)
                          in all contraryOrOblique onlyPerfectMotions

--The interval of a tenth should not be exceeded between two adjacent parts unless by necessity.
limitInterval :: [Int] -> [Int] -> Bool
limitInterval a b = let intervals = absIntervals a b
                        lessThanTenth i = i <= 16 -- a Major Tenth is 16 semitones
                     in all lessThanTenth intervals

--Use no unisons except at the beginning or end.
unisonOnlyBeginOrEnd :: [Int] -> [Int] -> Bool
unisonOnlyBeginOrEnd a b = let intervals = absIntervals a b
                               middle = (init . tail) intervals
                            in all (>0) middle

--Avoid parallel fifths or octaves between any two parts;
avoidParallelFifthsOrOctaves :: [Int] -> [Int] -> Bool
avoidParallelFifthsOrOctaves a b = let ms = motions a b
                                       isParallelFifthOrOctave (x,y) = abs x == octave || abs x == perfectFifth && x==y
                                       pfo = filter isParallelFifthOrOctave ms
                                    in null pfo

--and avoid "hidden" parallel fifths or octaves: that is, movement by similar motion to a perfect fifth or octave, unless one part (sometimes restricted to the higher of the parts) moves by step.


--Avoid moving in parallel fourths.
--(In practice Palestrina and others frequently allowed themselves such progressions, especially if they do not involve the lowest of the parts.)
avoidParallelFourths :: [Int] -> [Int] -> Bool
avoidParallelFourths a b = let ms = motions a b
                               isParallelFourth (x,y) = abs x == perfectFourth && x==y
                               pfo = filter isParallelFourth ms
                            in null pfo


allEqualToFirst :: [Int] -> Bool
allEqualToFirst ns = let alleq x = x == head ns
                      in all alleq ns

notAllEqual :: [Int] -> Bool
notAllEqual = not . allEqualToFirst

--Do not use an interval more than three times in a row.
lessThan3RepeatedIntervals :: [Int] -> [Int] -> Bool
lessThan3RepeatedIntervals a b = let intervals = absIntervals a b
                                     fours = windows 4 intervals
                                     onlyFours = filter (\t -> length t == 4) fours
                                  in all notAllEqual onlyFours

--Avoid having any two parts move in the same direction by leap
noLeapsInSameDirection :: [Int] -> [Int] -> Bool
noLeapsInSameDirection a b = let ms = motions a b
                                 similarLeaps (x,y) = (abs x>2 && abs y>2) && ((x>0 && y>0) || (x<0 && y<0))
                                 sls = filter similarLeaps ms
                              in null sls

--Avoid dissonant intervals between any two parts: major or minor second, major or minor seventh, any augmented or diminished interval, and perfect fourth (in many contexts).

noDissonantIntervals :: [Int] -> [Int] -> Bool
noDissonantIntervals a b = let intervals = Set.fromList $ absIntervals a b
                               inter = Set.intersection intervals dissonantIntervals
                            in Set.size inter == 0

leapOnlyByConsonantInterval :: [Int] -> Bool
leapOnlyByConsonantInterval ns = let ms = direction ns
                                     leaps = filter (\x -> abs x > 2) ms
                                     absLeaps = map abs leaps
                                     cleaps = filter isConsonantInterval absLeaps
                                  in length cleaps == length absLeaps

climaxIsConsonantWithScaleOne :: [Int] -> Bool
climaxIsConsonantWithScaleOne ns = undefined

--Attempt to use up to three parallel thirds or sixths in a row.
repeatParallelThirdsorSixths :: [Int] -> [Int] -> Bool
repeatParallelThirdsorSixths a b = undefined

isValidFirstSpeciesCounterpoint :: [Int] -> [Int] -> Bool
isValidFirstSpeciesCounterpoint a b = noDissonantIntervals a b
    && noLeapsInSameDirection a b
    && avoidParallelFourths a b
    && avoidParallelFifthsOrOctaves a b
    && unisonOnlyBeginOrEnd a b
    && approachPerfection a b
    && beginAndEndPerfectly a b
    && mostlyContrary a b
    && limitInterval a b
    && lessThan3RepeatedIntervals a b
    && mostlySteps a
    && mostlySteps b
    && leapOnlyByConsonantInterval a
    && leapOnlyByConsonantInterval b

data CounterpointReport = CounterpointReport {
    isValidFirstSpeciesR :: Bool,
    noDissonantIntervalsR :: Bool,
    noLeapsInSameDirectionR :: Bool,
    avoidParallelFourthsR :: Bool,
    avoidParallelFifthsOrOctavesR :: Bool,
    unisonOnlyBeginOrEndR :: Bool,
    approachPerfectionR :: Bool,
    beginAndEndPerfectlyR :: Bool,
    mostlyContraryR :: Bool,
    limitIntervalR :: Bool,
    lessThan3RepeatedIntervalsR :: Bool,
    mostlyStepsCantusFirmusR :: Bool,
    mostlyStepsCounterpointR :: Bool,
    onlyConsonantLeapsR :: Bool
} deriving (Show)

report :: [Int] -> [Int] -> CounterpointReport
report a b = CounterpointReport {
    isValidFirstSpeciesR = isValidFirstSpeciesCounterpoint a b,
    noDissonantIntervalsR = noDissonantIntervals a b,
    noLeapsInSameDirectionR = noLeapsInSameDirection a b,
    avoidParallelFourthsR = avoidParallelFourths a b,
    avoidParallelFifthsOrOctavesR = avoidParallelFifthsOrOctaves a b,
    unisonOnlyBeginOrEndR = unisonOnlyBeginOrEnd a b,
    approachPerfectionR = approachPerfection a b,
    beginAndEndPerfectlyR = beginAndEndPerfectly a b,
    mostlyContraryR = mostlyContrary a b,
    limitIntervalR = limitInterval a b,
    lessThan3RepeatedIntervalsR = lessThan3RepeatedIntervals a b,
    mostlyStepsCantusFirmusR = mostlySteps a,
    mostlyStepsCounterpointR = mostlySteps b,
    onlyConsonantLeapsR = leapOnlyByConsonantInterval a && leapOnlyByConsonantInterval b
}
