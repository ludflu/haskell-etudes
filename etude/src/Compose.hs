module Compose where

import qualified Data.List as DL
import qualified Data.Set as Set
import qualified Data.Vector as V
import Data.Maybe
import System.Random

import Counterpoint
import Euterpea
import Scales
import TraceMusic

data ComposedChunk = ComposedChunk { start :: Int, notes :: [Note] } deriving Show
data Location = Beginning | Middle | End deriving Eq

type Song = V.Vector Note


findLocation :: Int -> Int -> Location
findLocation here length = let middleLength = round $ 0.2 * fromIntegral length
                               startMiddle = round $ 0.4 * fromIntegral length
                            in findLocation' here startMiddle (startMiddle + middleLength)

findLocation' :: Int -> Int -> Int -> Location
findLocation' here startMiddle endMiddle
  | here < startMiddle = Beginning
  | here > endMiddle = End
  | otherwise = Middle

findNewLocation' :: Location -> Location
findNewLocation' here
  | here == Beginning = End
  | here == End = Beginning
  | otherwise = End  -- could also be beginning  - maybe choose randomly

findNewLocation :: Location -> Int -> Int
findNewLocation oldLocation length = let newLocation = findNewLocation' oldLocation
                                      in case newLocation of
                                      Beginning -> 2
                                      End -> length - 3
                                      Middle -> round  (fromIntegral length / 2.0)

toUnity :: Int -> Int
toUnity i 
  | i > 0 = 1
  | i < 0 = -1
  | i == 0 = 0


getConsonantNotesAbove :: Note -> [Note]
getConsonantNotesAbove note = map (+note) (Set.toList consonantIntervals)

pickOnDirection :: Note -> [Note] -> [Note] -> [Note]
pickOnDirection n above below
  | n >0 = above
  | n <0 = below
  | otherwise = above

--based on the last two notes of the cantusFirmus
--start from a note that is a consonant interval above the penultimate note
--move in contrary motion to a final note one octave above the last note of the cantusFirmus
composeEnd :: Note -> Note -> (Note,Note)
composeEnd last penultimate = let cantusDirection = penultimate - last --we want to move in contrary motion
                                  counterLast = last + octave  -- last note of counterpoint should be on scale degree 1
                                  counterPenCandidates = getConsonantNotesAbove penultimate
                                  above = filter (> counterLast) counterPenCandidates
                                  below = filter (< counterLast) counterPenCandidates
                                  candidates = pickOnDirection cantusDirection above below  --approach perfect constance of octave w/ contrary motion
                                  counterPen = head candidates   -- note partial function! please fix
                               in (counterPen, counterLast)

--returns the penultimate element from a list
pen :: [a] -> a
pen = head . tail . reverse

enumerateVariations :: Song -> [[ComposedChunk]] -> [Song]
enumerateVariations base proposals = let permutations = sequence proposals
                                         baseSongF = applyChunks base
                                         songs = map baseSongF permutations
                                      in songs

applyChunks :: Song -> [ComposedChunk] -> Song
applyChunks = foldl replaceComposedChunk

replaceComposedChunk :: Song -> ComposedChunk -> Song
replaceComposedChunk song chunk = replaceChunk song (start chunk) (notes chunk)

replaceChunk :: Song -> Int -> [Note] -> Song
replaceChunk music start replacement  = let indxs = V.fromList [start..(V.length music - 1)]
                                            patch = V.zip indxs $ V.fromList replacement
                                         in V.update music patch

-- given a cantus firmus, compose an ending, return new incomplete counterpoint
composeCounterpointEnding :: Song -> ComposedChunk
composeCounterpointEnding cantusFirmus = let cantusLen = length cantusFirmus
                                             lastNote = V.last cantusFirmus
                                             penultimate = pen $ V.toList cantusFirmus
                                             (cpen,clast) = composeEnd lastNote penultimate
                                          in ComposedChunk {start = cantusLen-2, notes = [cpen,clast]}


composeCounterpointBegining :: Song -> ComposedChunk
composeCounterpointBegining cantusFirmus = let cantusStart = V.head cantusFirmus
                                               counterStart = cantusStart + majorThird --could be 1, 3, 5
                                            in ComposedChunk{ start=0, notes=[counterStart] }


convertToMusic :: Dur -> Song -> [Music Pitch]
convertToMusic dur song = let abspitches = V.toList song
                              notemaker i = note dur $ pitch i
                           in map notemaker abspitches


zipWithIndex :: [a] -> [(a, Int)]
zipWithIndex as = zip as [0..]

findHighpoint :: Song  -> (Int,Int)
findHighpoint song = let notes = V.toList song
                         indexed = zipWithIndex notes
                         top = maximum notes
                         indx = DL.find (\(n,i) -> n == top) indexed
                      in fromMaybe (top,0) indx


composeCounterpointClimax :: Song -> ComposedChunk
composeCounterpointClimax cantusFirmus = let (cantusHighNote,index) = findHighpoint cantusFirmus
                                             hploc = findLocation index $ length cantusFirmus
                                             counterpointHighPoint = findNewLocation hploc $ length cantusFirmus
                                             hpCompanionNote = cantusFirmus V.! counterpointHighPoint
                                             candidates = getConsonantNotesAbove hpCompanionNote
                                             aboveCantus = filter (> cantusHighNote) candidates --TODO this is unneeded, right?
                                             --TODO must also be consonant with scale degree 1
                                             counterpointHighNote = maximum aboveCantus
                                          in ComposedChunk{ start=counterpointHighPoint, notes=[counterpointHighNote]}

composeGap :: Note -> Int -> Note -> Int -> Song -> IO ComposedChunk
composeGap startNote startIndx endNote endIndx cantusFirmus = do let distance = endIndx-startIndx+1
                                                                     musicSection = V.toList $ V.slice startIndx distance cantusFirmus
                                                                     legalNotes = map getConsonantNotesAbove musicSection
                                                                 path <- traceLegalPath startNote endNote distance legalNotes
                                                                 return ComposedChunk{start=startIndx,notes=path}

composeCounterpoint :: [Music Pitch] -> IO [Music Pitch]
composeCounterpoint cantusFirmus = do let cantus = V.fromList $ toPitches cantusFirmus
                                          ending = composeCounterpointEnding cantus
                                          beginning = composeCounterpointBegining cantus
                                          climax = composeCounterpointClimax cantus
                                      buildup <- composeGap (head $ notes beginning) 1 (head $ notes climax) (start climax) cantus
                                      denouement <- composeGap (head $ notes climax) (start climax) (head $ notes ending) (length cantusFirmus - 1) cantus
                                      let initialC = V.replicate (length cantusFirmus) (-1)
                                          patches = [beginning, buildup, climax, denouement, ending]
                                          composed = applyChunks initialC patches

                                      print cantus
                                      print composed
                                      return $ convertToMusic hn composed
