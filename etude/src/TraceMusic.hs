module TraceMusic where

import System.Random
import Control.Monad
import qualified Data.Set as Set
import Euterpea

import Data.Sort
import Control.Arrow

import Counterpoint

valsAllowed :: Int -> Bool
valsAllowed v = True

getRandoms :: Int -> Int -> Int -> IO [Int]
getRandoms low high count = mapM (\x -> getStdRandom (randomR (low,high))) [1..count]

inbetween :: Int -> Int -> [Int]
inbetween start end
  | start < end = [start..end]
  | end > start = reverse [start..end]
  | otherwise = []

toFloat :: Int -> Float
toFloat = fromIntegral


augment :: [a] -> [Int] -> [Int] -> [a]
augment seq insertIdx copyIdx = let copyvals = map (\i -> seq !! i) copyIdx
                                    _copyIdx = map (\i -> toFloat i  + 0.5 ) insertIdx
                                    toAdd = zip copyvals _copyIdx
                                    pairs = zip seq [0..]
                                    pairf = map (second toFloat) pairs
                                    psorted = sortOn snd (pairf ++ toAdd)
                                 in map fst psorted


getRef :: Int -> Int -> Int
getRef lim i
  | i <= 0 = i + 1
  | i >= lim = i - 1
  | otherwise = i

stretch :: [Int] -> Int -> IO [Int]
stretch seq targetLength = do let diff = targetLength - length seq
                              indexesToAddAt <- getRandoms 1 (length seq - 3) diff
                              let indexsToAddFrom = map (getRef (length seq - 3) ) indexesToAddAt
                              return $ augment seq indexesToAddAt indexsToAddFrom

excise :: [Int] -> [Int] -> [Int]
excise seq toDrop = let pairs = zip seq [0..]
                        dropSet = Set.fromList toDrop
                        onlyGoodPairs = filter (\(v,i) -> not (Set.member i dropSet)) pairs
                     in map fst onlyGoodPairs

shorten :: [Int] -> Int -> IO [Int]
shorten seq targetLength = do let diff = length seq - targetLength
                              toExcise <- getRandoms 1 (length seq-2) diff
                              return $ excise seq toExcise

fit :: [Int] -> Int -> IO [Int]
fit seq targetLength
  | null seq = return seq
  | length seq > targetLength = shorten seq targetLength
  | length seq < targetLength = stretch seq targetLength
  | otherwise = return seq

tracePath ::  Note -> Note -> Int -> IO [Note]
tracePath start end dist = let totalPath = inbetween start end
                            in fit totalPath dist


minimizeWith :: Ord b => (a -> b) -> [a] -> a
minimizeWith score seq = let scores = map score seq
                             pairs = zip seq scores
                             sortedP = sortOn snd pairs
                          in (fst . head) sortedP

absdiff :: Int -> Int -> Int
absdiff a b = abs (a - b)

-- for each value in the sequence, pick the value in the allowed set that is closest to it
mimimizePathError :: [Int] -> [[Int]] -> [Int]
mimimizePathError seq allowed = let scorers = map absdiff seq
                                    minimized = zipWith minimizeWith scorers allowed
                                 in minimized



traceLegalPath :: Note -> Note -> Int -> [[Note]] -> IO [Note]
traceLegalPath start end dist legal = do naivePath <- tracePath start end dist
                                         return $ mimimizePathError naivePath legal


