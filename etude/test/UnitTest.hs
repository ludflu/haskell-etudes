module Main where

import Test.Tasty
import Test.Tasty.HUnit
import Counterpoint
import Scales
import ComposeUnitTests

counterpointTests :: TestTree
counterpointTests = testGroup "Counterpoint tests"
    [
        directionTest,
        motionTest,
        motionTest2,
        motionTest3,
        beginAndEndPerfectlyTest,
        beginAndEndPerfectlyTest2,
        approachPerfectionTest,
        approachPerfectionTest2,
        approachPerfectionTest3,
        noParallelFifthsOrOctave,
        noSimilarLeaps,
        noSimilarLeaps2,
        noSimilarLeaps3,
        isMostlySteps,
        isMostlySteps2,
        onlyConsonantLeaps,
        onlyConsonantLeaps2,
        isValidCounterpoint,
        composeTests
    ]

directionTest :: TestTree
directionTest = testCase "direction" $
            direction [1..5] @?= [1,1,1,1]

motionTest :: TestTree
motionTest = testCase "Motion Parallel" $
    motionTypes [1..5] [1..5] @?= [Parallel, Parallel, Parallel, Parallel]

motionTest2 :: TestTree
motionTest2 = testCase "Motion Contrary" $
    motionTypes [1..5] [5,4,3,2,1] @?= [Contrary, Contrary, Contrary, Contrary]

motionTest3 :: TestTree
motionTest3 = testCase "Motion Mixed" $
    motionTypes [1,2,3,4,5] [1,-2, 1, -2, 1] @?= [Contrary, Similar, Contrary, Similar]

beginAndEndPerfectlyTest :: TestTree
beginAndEndPerfectlyTest = testCase "BeginAndEnd" $
    beginAndEndPerfectly [0,1,2,3,7] [12,3,2,1,0] @?= True

beginAndEndPerfectlyTest2 :: TestTree
beginAndEndPerfectlyTest2 = testCase "BeginAndEndImperfectly" $
    beginAndEndPerfectly [1,2,3,6,7] [12,3,2,1,0] @?= False

approachPerfectionTest ::  TestTree
approachPerfectionTest = testCase "approachPerfectionTest" $
    approachPerfection [1,2,3,0] [3,2,1,5] @?= True

approachPerfectionTest2 ::  TestTree
approachPerfectionTest2 = testCase "NotApproachPerfectionTest" $
    approachPerfection [1,2,3,0] [1,2,3,5] @?= False

approachPerfectionTest3 ::  TestTree
approachPerfectionTest3 = testCase "ApproachPerfectionObliquelyTest" $
    approachPerfection [1,3,3,0] [1,2,3,5] @?= True


noParallelFifthsOrOctave :: TestTree
noParallelFifthsOrOctave = testCase "noParallelFifthsOrOctave" $
    avoidParallelFifthsOrOctaves [1,2,3,10] [4,3,2,9] @?= False

noParallelFifthsOrOctave2 :: TestTree
noParallelFifthsOrOctave2 = testCase "noParallelFifthsOrOctave" $
    avoidParallelFifthsOrOctaves [1,2,3,10] [4,3,2,10] @?= True

noSimilarLeaps :: TestTree
noSimilarLeaps = testCase "noSimilarLeaps" $
    noLeapsInSameDirection [1,2,3,4] [4,3,2,1] @?= True

noSimilarLeaps2 :: TestTree
noSimilarLeaps2 = testCase "noSimilarLeaps2" $
    noLeapsInSameDirection [1,2,6,5] [2,3,7,6] @?= False

noSimilarLeaps3 :: TestTree
noSimilarLeaps3 = testCase "noSimilarLeaps3" $
    noLeapsInSameDirection [1,2,6,5] [2,3,-7,-6] @?= True

isMostlySteps :: TestTree
isMostlySteps = testCase "isMostlySteps" $
    mostlySteps [1,2,4,6,8,12] @?= True

isMostlySteps2 :: TestTree
isMostlySteps2 = testCase "isMostlySteps2" $
    mostlySteps [1,5,8,11,10,14] @?= False

onlyConsonantLeaps :: TestTree
onlyConsonantLeaps = testCase "onlyConsonantLeaps" $
    leapOnlyByConsonantInterval [0,perfectFifth,0,octave,0,majorThird] @?= True

onlyConsonantLeaps2 :: TestTree
onlyConsonantLeaps2 = testCase "onlyConsonantLeaps2" $
    leapOnlyByConsonantInterval [0,majorSecond,0,tritone,0,perfectFifth,0,majorSeventh] @?= False

isValidCounterpoint :: TestTree
isValidCounterpoint = testCase "isValidCounterpoint" $
    let cantusFirmus = [67,65,67,69,71,74,72,72,74,76,74,69,72,71,72] 
        counterpoint = [60,62,64,60,67,65,64,69,67,60,62,65,64,62,60]
     in isValidFirstSpeciesCounterpoint cantusFirmus counterpoint @?= True

main :: IO ()
main = defaultMain counterpointTests
