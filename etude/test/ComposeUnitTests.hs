module ComposeUnitTests where

import Test.Tasty
import Test.Tasty.HUnit
import Counterpoint
import Scales
import Compose
import qualified Data.Vector as V

composeTests :: TestTree
composeTests = testGroup "Counterpoint tests"
    [
        highpointTest, replaceChunkTest, applyChunksTest, applyNullChunkTest
    ]


m = [1..10] ++ [9..12] ++ reverse [1..11]
vm = V.fromList m

highpointTest :: TestTree
highpointTest = testCase "highpoint" $
            findHighpoint vm @?= (12,13)


song = V.fromList [1..10]
test = V.toList $ replaceChunk song 5 [0,0,0]
replaceChunkTest :: TestTree
replaceChunkTest = testCase "replaceChunk" $
    test @?= [1,2,3,4,5,0,0,0,9,10]


chunk1 = ComposedChunk{start=1,notes=[-1,-1]}
chunk2 = ComposedChunk{start=8,notes=[-2,-2]}

testA = V.toList $ applyChunks song [chunk1, chunk2]
applyChunksTest :: TestTree
applyChunksTest = testCase "applyChunks" $
    testA @?= [1,-1,-1,4,5,6,7,8,-2,-2]


nullchunk = ComposedChunk{start=5,notes=[]}
testNull = V.toList $ applyChunks song [nullchunk]
applyNullChunkTest :: TestTree
applyNullChunkTest = testCase "applyNullChunk" $
    testNull @?= [1..10]
