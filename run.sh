#!/usr/bin/env bash
docker build . -t ubudocker:latest
docker run -it \
    --entrypoint "/bin/bash" \
    --device /dev/snd \
    --mount src="$(pwd)",target=/ubudocker,type=bind \
    ubudocker:latest
