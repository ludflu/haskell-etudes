from ubuntu:latest

RUN apt-get update && apt-get -y install mpg123 timidity neovim wget

RUN	apt-get update && apt-get install -y \
	ca-certificates \
	wget \
    timidity \
    neovim \
    mpg123 \
	dirmngr \
	gnupg \
	alsa-utils \
    libasound2-dev \
	libgl1-mesa-dri \
	libgl1-mesa-glx \
	libpulse0 \
	xdg-utils \
    cabal-install \
    tmux \
    --no-install-recommends \
	&& rm -rf /var/lib/apt/lists
RUN cabal update && cabal install Euterpea
